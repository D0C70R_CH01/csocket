﻿package com.doctorChoi
{
	/*code by DoctorChoi (https://gitlab.com/DoctorChoi) */

	import flash.net.XMLSocket;
	import flash.utils.ByteArray;
	import flash.events.Event;
	import com.doctorChoi.cSocketEvent;
	import flash.errors.IOError;
	import flash.events.DataEvent;
	import flash.events.EventDispatcher;
	//import 끗

	public class cSocket extends EventDispatcher
	{
		public static const WELCOME: String = "welcome";
		public static const BYE: String = "bye";

		private var _host: String;
		private var _port: int;
		private var _xs: XMLSocket;
		private var _isCon: Boolean = false;
		private var _func: Function;
		//변수선언 끗

		public function cSocket(port: int, host: String = "localhost"): void
		{
			if (port < 1024)
			{
				throw new Error("cSocket: #0 : 1024 미만의 포트는 사용할 수 없음", 0);
				return void;
			}
			_host = host;
			_port = port;
			_xs = new XMLSocket();
		}
		//생성자 끗

		public function connect(): void
		{
			if (!_isCon)
			{
				_xs.connect(_host, _port);
				_xs.addEventListener(Event.CONNECT, _fcon);
				_xs.addEventListener(DataEvent.DATA, _fdata);
			}
			else
			{
				throw new Error("cSocket: #-1 : 이미 연결되어 있음", -1);
			}
		}
		//연결하는 함수 끗

		public function get isConnected(): Boolean
		{
			return _isCon;
		}
		//연결되어있는지 보여주는 함수 끗

		public function disconnect(): void
		{
			if (_isCon)
			{
				_xs.close();
				_xs.addEventListener(Event.CLOSE, _fclo);
				_xs.removeEventListener(DataEvent.DATA, _fdata);
			}
			else
			{
				throw new Error("cSocket: #-2 : 연결되어있지 않음", -2);
			}
		}
		//연결 끊는 함수 끗

		private function _fcon(e: Event): void
		{
			_isCon = true;
			_xs.removeEventListener(Event.CONNECT, _fcon);
			_fmakeEvent(
			{
				type: WELCOME
			});
		}
		//이벤트리스너용함수 1호기 끗

		private function _fclo(e: Event): void
		{
			_isCon = false;
			_xs.removeEventListener(Event.CLOSE, _fclo);
			_fmakeEvent(
			{
				type: BYE
			});
		}
		//이벤트리스너용 함수 2호기 끗

		public function send(type: String, data: Object): Boolean
		{
			var _tmp: Object = new Object();
			_tmp._type = type;
			_tmp._data = data;
			try
			{
				_xs.send(JSON.stringify(_tmp));
			}
			catch (e: IOError)
			{
				return false;
			}
			return true;
		}
		//데이터 던지는 함수 끗

		public function on(func: Function): void
		{
			if (_func == null)
			{
				_func = func;
				this.addEventListener(cSocketEvent.RECEIVE, _func);
			}
			else
			{
				throw new Error("cSocket: # -3 : 이미 연결된 함수가 있음", -3);
			}
		}
		//이벤트리스너 달아주는 함수 끗

		public function off(): void
		{
			if (_func != null)
			{
				this.removeEventListener(cSocketEvent.RECEIVE, _func);
				_func = null;
			}
			else
			{
				throw new Error("cSocket: # -4 : 연결된 함수가 없음", -4);
			}
		}
		//이벤트리스너 떄주는 함수 끗

		private function _fdata(e: DataEvent): void
		{
			trace(e.data);
			_fmakeEvent(
			{
				type: JSON.parse(e.data)._type,
				data: JSON.parse(e.data)._data
			});
		}
		//이벤트리스너용 함수 3호기 끗

		private function _fmakeEvent(_dt: Object): void
		{
			dispatchEvent(new cSocketEvent(cSocketEvent.RECEIVE, _dt));
		}
		//이벤트 제조기 끗
	}
}