﻿package com.doctorChoi
{
	/*code by DoctorChoi (https://gitlab.com/DoctorChoi) */

	import flash.events.Event;
	//import 끗

	public class cSocketEvent extends Event
	{
		public static const RECEIVE: String = "receive";
		//상수선언 끗

		private var _data: Object;

		public function cSocketEvent(type: String, info: Object, bubbles: Boolean = false, cancelable = false): void
		{
			super(type, bubbles, cancelable);
			_data = info;
		}
		//생성자 끗

		override public function clone(): Event
		{
			return new cSocketEvent(type, _data, bubbles, cancelable);
		}
		//재정의 끗

		public function get data(): Object
		{
			if (_data.data == undefined)
			{
				return null;
			}
			else
			{
				return _data.data;
			}
		}
		//데이터 던져주는 함수 끗

		public function get info(): String
		{
			return _data.type;
		}
		//정보 던져주는 함수 끗
	}
}