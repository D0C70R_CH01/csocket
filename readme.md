# README

XMLSocket를 그냥 쓰기 편하게 건들였을 뿐입니다.  
README에서 비틱냄새가 너무 심하게나서 README만 조금 수정했습니다.  
버그가 굉장히 많을겁니다 아마...  
src에 typescript로 만든 테스트용 서버가 동봉되어있으니 쓰실분은 35번째줄을 바꿔서 쓰시면 됩니다.  
람쥐  

20181126: 예제 추가 Adnbe Animate CC 2018로 작성됨  

## 그래서 어떻게 쓰는데요

```as
import com.doctorChoi.cSocket;
import com.doctorChoi.cSocketEvent;

var cs: cSocket = new cSocket("<포트>", "<호스트>");
//객체 선언

cs.connect();
//연결

cs.on(function (e: cSocketEvent): void
{
    //...
});
//이벤트

cs.send("<info>", "<data>");
//전송
```

## 설명서

### cSocket

```as
public function cSocket(port: int, host: String = "localhost"): void
```

cSocket 객체를 만듭니다. port는 1024 이상. host는 비워두면 로컬호스트로 연결.  
생성후 자동으로 연결되지 않습니다.

```as
public function connect(): void
```

연결합니다.  
연결에 성공시 {info: cSocket.WELCOME, data:null}로 이벤트가 발생합니다.

```as
public function get isConnected(): Boolean
```

연결되어 있는지 알려줍니다.

```as
public function disconnect(): void
```

연결을 끊습니다.
연결 을 끊을시 {info: cSocket.BYE, data:null}로 이벤트가 발생합니다.

```as
public function send(type: String, data: Object): Boolean
```

데이터를 던지고 성공적으로 던져지면 true를, 아니면 false를 반환합니다.

```as
public function on(func: Function): void
```

이벤트리스너를 달아줍니다.

```as
public function off(): void
```

이벤트리스너를 때냅니다.

### cSocketEvent

```as
public function cSocketEvent(type: String, info: Object, bubbles: Boolean = false, cancelable = false): void
```

cSocketEvent 객체를 만듭니다.

```as
public function get data(): Object
```

받은 데이터의 data 부분을 받아옵니다.

```as
public function get info(): String
```

받은 데이터의 type 부분을 가져옵니다.  