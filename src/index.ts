/*code by DoctorChoi (https://gitlab.com/DoctorChoi) */

import * as net from "net";
//import 끗

const DATA: string = "data";
//상수선언 끗

var _so: net.Socket;
//변수선언 끗

var server: net.Server = net.createServer((socket:net.Socket) => 
    {
        _so = socket;
        _so.on(DATA, _fdata);
    }
);
//서버 객체 생성 끗

function _fdata(e: any): void
{
    console.log(e);
    _so.write(e);
}
//데이터 들어오면 호출되는 함수 끗

server.listen(8420, "0.0.0.0");
//서버온